# Design

The design of the Wifi Light project is inspired by two articles published in
the "Hackable Magazine" of July-August 2015 :

* One is introducing the ESP-8266 and the ability of the Arduino IDE to replace
the original firmware by a customized Web client program changing the state of
the GPIO2 depending on data provided by a predefined Web site.

* Another one is giving advices and providing solution to safely control high
voltage circuit from a low voltage one (based for instance on an Arduino device)

The union of the two concepts discussed in these articles provides the basics of
the WIFI light electronic device. Its design is illustrated in the following
diagram:

![Wifi Light Electronic Device Design](res/schematic.png)

This diagram highlights three parts in the device design :

* On the left of the diagram, the ESP-8266 with its own battery and a direct
wiring for control ports to set the micro-controller is a "usage" mode.

* In the center of the diagram, the anode of the optocoupler 4N35 is connected
to the GPIO2 of the ESP-8266 through a switch that enables a manual switch-off
of the light.

* On the right of the diagram, the collector and the emitter of the optocoupler
and connected to a PNP transistor that controls an AZ850 polarized relay in
charge of switching the light on or off. As for the ESP-8266 part, this part of
of the circuit has its own battery to avoid any wired connection between the
control part and the command part.
