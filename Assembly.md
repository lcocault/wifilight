# Assembly

## PCB assembly design

The design of the PCB assembly, prepared thanks to the Fritzing software, is
illustrated in the following diagram:

![PCB assembly design](res/pcb.png)


## PCB preparation

A 50mm-90mm PCB is used to implement the device. To optimize the surface
 of PCB used, sections have to be done in order to isolate parts of the PCB. The
following picture illustrates the corresponding sections:

![PCB preparation](res/raw-pcb.png)


## Assembly

Most of the electronic components are directly soldered on the PCB. However,
the most fragile components are inserted into supports that are previously
soldered. The final result is illustrated in the following picture:

![Final PCB](res/full-pcb.png)


## Issues

The final assembly revealed a few issues that led to a spare of space on the PCB:

* In the initial version of the device, two lithium batteries were used. But this
approach was not convenient because the ESP8266 requires up to 250 mA of
input current; and a lithium battery can hardly deliver more than 30 mA.

* An error of wiring led to unsolder and resolder components multiple times.
Applying hot temperature on the PCB caused dommages on one track of the PCB
which required additional wires to "patch" the circuit.
