# Scope

The Wifi Light project is an ESP-8266 based project that consists of connecting
a light to an web site in order to switch it on or off from any mobile device.

## Principles and architecture

The ESP-8266 chip enables Wifi communications. It is based on a microcontroller
provided with a firmware enabling a device to send or receive data through the
TX and RX pins. The Arduino community recently extended the Arduino IDE to get
the capability to reprogram the default firmware. WIFI libraries and ESP-8266
drivers are provided to ease this.

The "Wifi Light" project takes advantage from this possibility to control a
home light from any peripheral connected to Internet. The architecture of this
project is detailed [here](Architecture.md).


## Web service

The architecture of the project is based on a Web Service providing the
following capabilities:

* Get the current state of the light

* Switch on the light

* Switch off the light

### Design

The constraints that drove the Wifi Light web service design are:

* A simple solution, to ease the detailed design and implementation

* A solution based on standard technologies, to host the service for free

A page detailing the Web Service design will be added __soon__.

### Implementation

The Web Service has been designed to be simple to implement.  In the end, it
only consists of four PHP files:

* __[CouchSimple.php](php/CouchSimple.php)__ provides basic services to query
the database. This script is used by the three other pages.

* __[LightValue.php](php/LightValue.php)__ delivers the current state (or value)
of the light. This page, that simply displays ON/OFF depending on the light
state, is read by the electronic device to physically switch the light on or
off.

* Other scripts will be documented __soon__.


## Electronic device

The electronic device is in charge of physically interacting with the light, in
other words... switching it on or off. The device is composed of two main parts:

* A Wifi component to connect to the Web Service, get the light state ("On" or
"Off"), and accordingly put an output pin to "High" or "Low".

* A command component that physically switch on or off the light depending on
the value of the Wifi component output.

### Design

The main components of the electronic device are listed below:

* An ESP-8266 to connect and query the Web Service in order to deliver a signal
to the command equipments

* An optocoupler to physically separate the ESP-8266 from the high voltage
circuit

* A relay to control the lamp current.
 
The design of the "Wifi Light" project is detailed [here](Design.md).

### Implementation

The implementation of the electronic device has been achieved through the
following steps:

* Prototype is documented [here](Prototype.md)

* Circuit assembly is documented [here](Assembly.md)

* Packaging will be documented __soon__


#License

All the elements of the project are licensed by Laurent COCAULT under the
Apache License Version 2.0. A copy of this license is provided in the
[LICENSE.txt](LICENSE.txt) file.

This project includes resources under CC license provided by:

* [Tango! Desktop Project](http://tango.freedesktop.org/Tango_Desktop_Project)

* [KSIOM](https://commons.wikimedia.org/wiki/User:Ksiom)

# Content

The fzz directory contains the Fritzing files.
The ino directory contains the Arduino source files.
The php directory contains the Web site source files.
