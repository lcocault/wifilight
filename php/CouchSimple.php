/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<?php

  // Connection parameters
  $options['host'] = "couchdb-cocault.alwaysdata.net"; 
  $options['port'] = 5984;
  
  // Simple instance
  $couch = new CouchSimple($options);

  /*
   * Simple access API to a CouchDB database
   */
  class CouchSimple {

    /*
     * Constructor
     * @param options Options of the connection
     */
    function CouchSimple($options) {
      foreach($options AS $key => $value) {
        $this->$key = $value;
      }
    } 

    /*
     * Send a request to the CouchDB database
     * @param method Method of
     */
    function send($method, $url, $post_data = NULL) {
      $s = fsockopen($this->host, $this->port, $errno, $errstr); 
      if(!$s) {
       echo "$errno: $errstr\n"; 
       return false;
      }

      $request = "$method $url HTTP/1.0\r\nHost: $this->host\r\n"; 

      if ($this->user) {
        $request .= "Authorization: Basic ".base64_encode("$this->user:$this->pass")."\r\n"; 
      }

      if($post_data) {
        $request .= "Content-Length: ".strlen($post_data)."\r\n\r\n"; 
        $request .= "$post_data\r\n";
      }  else {
        $request .= "\r\n";
      }

      fwrite($s, $request); 
      $response = ""; 

      while(!feof($s)) {
        $response .= fgets($s);
      }

      list($this->headers, $this->body) = explode("\r\n\r\n", $response); 
      return $this->body;
    }

  }
?>
