<!-- Copyright 2015 Laurent COCAULT
  -- Licensed to Laurent COCAULT under one or more contributor license agreements.
  -- See the NOTICE file distributed with this work for additional information
  -- regarding copyright ownership. Laurent COCAULT licenses this file to You
  -- under the Apache License, Version 2.0 (the "License"); you may not use this
  -- file except in compliance with the License.  You may obtain a copy of the
  -- License at
  --
  --   http://www.apache.org/licenses/LICENSE-2.0
  --
  -- Unless required by applicable law or agreed to in writing, software
  -- distributed under the License is distributed on an "AS IS" BASIS,
  -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  -- See the License for the specific language governing permissions and
  -- limitations under the License.
  --> 
<HTML>
  <HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  </HEAD>
  <BODY>
    <?php

      /* Include the CouchDB simple access */
      include "CouchSimple.php";

      /* Get the object 001 (should be parameterized) */
      $resp = $couch->send("GET", "/cocault_iot/001");
      $data = json_decode($resp);
      
      /* Change the state to ON */
      $data->state = "ON";

      /* Store the modified object */
      $resp = $couch->send("PUT", "/cocault_iot/001", json_encode($data));
    ?>
    
    <A href="LightSwitchOff.php">
      <IMG src="on.png" />
    </A>
    
  </BODY>
</HTML>
