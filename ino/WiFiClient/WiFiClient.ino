/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  This sketch sends data via HTTP GET requests to data.sparkfun.com service.
 *
 *  You need to get streamId and privateKey at data.sparkfun.com and paste them
 *  below. Or just customize this script to talk to other HTTP servers.
 *
 */
#include <ESP8266WiFi.h>

/* WIFI connection parameters */
const char* ssid = "Bbox-C042674D";
const char* password = "E1112ECE6CAF12F5C74EF5EF46E2EA";

/* Web site hosting the state of the light */
const char* host = "cocault.alwaysdata.net";
const char* url = "/iot/LightValue.php";

/*
 * Initialize the ESP-8266 as a web client
 */
void setup() {

	// Connect to a WiFi network
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
	}
}

/*
 * Periodically check the state of the light
 */
void loop() {

	// Check frequency partially depends on that delay
	delay(4000);

	// Use WiFiClient class to create TCP connections
	WiFiClient client;
	const int httpPort = 80;
	if (!client.connect(host, httpPort)) {
		return;
	}

	// Send the request to the server
	client.print(
			String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + host + "\r\n"
					+ "Connection: close\r\n\r\n");

	// Wait for the answer: check frequency partially depends on that delay
	delay(1000);

	// Read all the lines of the reply from server and parse them
	while (client.available()) {

		// Read a line
		String line = client.readStringUntil('\r');

		if (line.endsWith("OFF")) {

			// Switch OFF
			pinMode(2, OUTPUT);
			digitalWrite(2, 0);

		} else if (line.endsWith("ON")) {

			// Switch ON
			pinMode(2, OUTPUT);
			digitalWrite(2, 1);
		}

	}
}

