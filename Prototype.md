# Prototype

## Bill of material

The selection of hardware components has been done thanks in the frame of the
prototyping activities. The resulting bill of material is introduced hereafter:

* 1 | ESP8266 Esp-01 | 4,20 euros

* 1 | CR2032 Lithium Battery Support | 1,10 euros

* 1 | CR2032 Lithium Battery 3V | 1,00 euros

* 1 | 3V DC Relay AZ850-3 | 2,90 euros

* 1 | E27 lamp slot | 1,00 euros

* 1 | 2LR6 battery coupler |  0,90 euros

* 1 | PC Board 90 mm x 50 mm, Step 2.54 mm | 1,90 euros

* 1 | BC557B PNP Transistor | 0,09 euros

* 1 | 4N35-000E DIP 6 Optocoupler | 0,50 euros

* 1 | 1N4007 Diode | 0,09 euros

* 1 | IC support 7.62 mm 8p | 0,15 euros

* 1 | IC support 7.62 mm 6p | 0,15 euros

* 1 | Linear switch 2 positions, 6 pins | 1,20 euros

* 1 | 1k resistor | 0,01 euros

* 1 | 220 ohms resistor | 0,01 euros

* 4 | 10k resistor | 0,01 euros each

* 1 | 50 cm wire | 0,20 euros

* 1 | Battery clip | 0,40 euros

Total amount: approx. 15,80 euros


## ESP8266 wiring

The wiring of the ESP8266 chip mainly consists of setting the pins to High or Low,
 using appropriate resistors. Only the GPIO2 pin is used to command the power
part. The prototyping of the ESP8266 wiring is illustrated in the following
picture:

![Control part - ESP8266 wiring](res/proto-control.png)


## Command part

The command part consists of three main components:

* The optocoupler connected to the GPIO2 of the ESP8266,

* The PNP transistor having its base connected to the optocoupler,

* The relay that drives the power and which is controlled by the transistor.

Connections between these components is illustrated in the following picture:

![Command part - Optocoupler-Relay wiring](res/proto-power.png)
