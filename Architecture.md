# Architecture

The architecture of the Wifi Light project is composed of three main parts:

* A Web Service, available from any device connected to the Internet, via an
Ethernet cable of a Wifi link.

* An Electronic Device that connects and query the Web Service via a Wifi
connection.

* Any client device (smartphone, tablet, desktop PC, laptop PC) having a Web
client to connect to the Web Service.

This architecture is illustrated in the following diagram

![Wifi Light Architecture](res/architecture.png)
